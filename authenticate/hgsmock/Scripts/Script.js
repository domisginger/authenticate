﻿//StartUp
$(function() {
    $('#registerForm').hide();

    // Declare a proxy to reference the hub.
    var hub = $.connection.broadcastHub;

    var regError = false;
    var userJson = "";
    var usernameText = "";

    // Create a function that the hub can call to broadcast messages.
    hub.client.broadcastAlert = function(from, text) {
        // Html encode display name and message.
        var encodedName = $('<div />').text(from).html();
        var encodedMsg = $('<div />').text(text).html();

        //Show the message
        alert(encodedName + ": " + encodedMsg);
    };

    hub.client.authResponce = function(authenticated) {
        if (authenticated) {
            $('body').css('background', 'lightgreen');
        } else {
            $('body').css('background', 'lightcoral');
        }
    };

    hub.client.registerSuccessful = function(success) {
        if (success && hasSessionStorage()) { //TODO: Eventually add cookie support aswell as sessionstorage
            // Add temp session of reg
            sessionStorage.register = userJson;

            // Redirect to main App
            $(location).attr('href', "/App.html");

        } else {
            alert("Registration Error - Please try again.");
        }
    };

    hub.client.findUserSuccessful = function(success, json) {
        if (success && hasSessionStorage()) { //TODO: Eventually add cookie support aswell as sessionstorage
            // Add temp session of reg
            sessionStorage.user = json;

            // Redirect to main App
            $(location).attr('href', "/App.html");

        } else {
            var el = $("#logUsernameText");
            el.css("color", "hsl(8, 70%, 54%)");
            el.text("Username not found!");
        }
    };

    hub.client.checkUserSuccessful = function(success) {
        var el = $("#regUsernameText");

        if (success) {
            el.css("color", "hsl(8, 70%, 54%)");
            el.text("Username already taken!");
            regError = true;
        } else {
            el.css("color", "white");
            el.text("Username ok");
            regError = false;
        }
    };

    $('#regSubmit').click(function() {
        if (!regError) {
            $.connection.hub.start().done(function() {
                var user = {
                    name: $("#regName").val(),
                    username: $("#regUsername").val(),
                    email: $("#regEmail").val()
                };

                var temp = { user: user };
                userJson = JSON.stringify(temp);

                hub.server.registerNewUser(userJson);

            });
        }
    });

    $('#logSubmit').click(function() {
        $.connection.hub.start().done(function() {
            hub.server.findUser($("#logUsername").val());
        });
    });

    $("#regUsername").change(function() {
        $.connection.hub.start().done(function() {
            hub.server.checkUser($("#regUsername").val());
        });
    });

    hasSessionStorage();
});

String.prototype.getHash = function() {
    var temp = CryptoJS.SHA3(this.toString());
    return temp.toString();
};

function showRegister() {
    $('#registerForm').slideDown(500);

    //TODO: scroll to $(#registerForm)
}

function hasSessionStorage() {
    if (typeof (Storage) !== "undefined") {
        return true;
    } else {
        alert("This browser does not support HTML5. In an endevor to keep upto date please upgrade or switch browsers. (E.g. Chrome, IE8+");
        return false;
    }
}