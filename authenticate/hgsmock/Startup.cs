﻿using System.Web.Http;
using Authenticate.Web;
using Authenticate.Web.App_Start;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof (Startup))]

namespace Authenticate.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
        }
    }
}