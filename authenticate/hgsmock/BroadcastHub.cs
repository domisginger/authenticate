﻿using System.Web.Script.Serialization;
using Authenticate.Web.ApplicationServices;
using Microsoft.AspNet.SignalR;

namespace Authenticate.Web
{
    // These call the clients Javascript methods AND receive them
    public class BroadcastHub : Hub
    {
        private readonly IBarcodeService Barcode;
        private readonly IPasswordService Password;

        private readonly IPinService Pin;
        private readonly IUserActionsService UserActions;

        public BroadcastHub()
        {
            UserActions = new MockUserActions();
            Password = new MockPassword();
            Pin = new MockPin();
            Barcode = new MockBarcode();
        }

        public void AuthenticatePassword(string pass, string userRef)
        {
            bool authenticated = Password.Validate(pass, userRef);

            AuthResponce(authenticated);
        }

        public void AuthenticatePin(string pin, string userRef)
        {
            bool authenticated = Pin.Validate(pin, userRef);

            AuthResponce(authenticated);
        }

        //TODO: Remove as not used now due to size of signalr
        //public void AuthenticateBarcode(string imageData, string userRef)
        //{
        //    var code = VideoActions.FindBarcode(imageData);
        //    var authenticated = Barcode.Validate(code, userRef);

        //    this.AuthResponce(authenticated);
        //}

        public void RegisterNewUser(string regDataJson)
        {
            bool success = UserActions.RegisterNewUser(regDataJson);

            RegisterSuccess(success);
        }

        public void GenerateQr(string username)
        {
            string userRef = UserActions.GetUserFromUsername(username).UserReference;

            string qr = Barcode.GenerateQr(userRef);
            if (qr != "")
            {
                GenerateQrSuccessful(true, qr);
            }
            else
            {
                GenerateQrSuccessful(false, "");
            }
        }

        public void FindUser(string username)
        {
            MockUser user = UserActions.GetUserFromUsername(username);
            if (user != null)
            {
                string userJson = new JavaScriptSerializer().Serialize(user);
                FindUserSuccess(true, userJson);
            }
            else
            {
                FindUserSuccess(false, "");
            }
        }

        public void CheckUser(string username)
        {
            MockUser user = UserActions.GetUserFromUsername(username);
            if (user != null)
            {
                CheckUserSuccess(true);
            }
            else
            {
                CheckUserSuccess(false);
            }
        }

        public void RegisterAuthTypes(string regDataJson)
        {
            bool success = UserActions.SetAuthenticationTypes(regDataJson);

            RegisterSuccess(success);
        }

        private void AuthResponce(bool authenticated)
        {
            Clients.Caller.authResponce(authenticated);
        }

        private void RegisterSuccess(bool success)
        {
            Clients.Caller.registerSuccessful(success);
        }

        private void FindUserSuccess(bool success, string userJson)
        {
            Clients.Caller.findUserSuccessful(success, userJson);
        }

        private void CheckUserSuccess(bool success)
        {
            Clients.Caller.checkUserSuccessful(success);
        }

        private void GenerateQrSuccessful(bool success, string qrImg)
        {
            Clients.Caller.generateQrSuccessful(success, qrImg);
        }
    }
}