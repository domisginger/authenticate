﻿angular.module("Authenticate").controller("RegPinCtrl",
[
    "$rootScope", "$scope",
    function($rootScope, $scope) {

        $scope.initialPin = "";
        $scope.pinNoticeText = "";


        $scope.init = function() {
            $scope.createGrid();
        };

        $scope.pinGrid = {};
        $scope.createRandomGrid = function() {
            var list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
            var randList = [];
            for (var i = 0; i < 10; i++) {
                var random = Math.floor(Math.random() * (list.length - 1));
                var a = list[random];
                list.splice(random, 1);
                randList.push(a);
            }

            $scope.pinGrid.pin1 = randList[0];
            $scope.pinGrid.pin2 = randList[1];
            $scope.pinGrid.pin3 = randList[2];
            $scope.pinGrid.pin4 = randList[3];
            $scope.pinGrid.pin5 = randList[4];
            $scope.pinGrid.pin6 = randList[5];
            $scope.pinGrid.pin7 = randList[6];
            $scope.pinGrid.pin8 = randList[7];
            $scope.pinGrid.pin9 = randList[8];
            $scope.pinGrid.pin10 = randList[9];
        };
        $scope.createGrid = function() {
            $scope.pinGrid.pin1 = "1";
            $scope.pinGrid.pin2 = "2";
            $scope.pinGrid.pin3 = "3";
            $scope.pinGrid.pin4 = "4";
            $scope.pinGrid.pin5 = "5";
            $scope.pinGrid.pin6 = "6";
            $scope.pinGrid.pin7 = "7";
            $scope.pinGrid.pin8 = "8";
            $scope.pinGrid.pin9 = "9";
            $scope.pinGrid.pin10 = "0";
        };

        $scope.enterPin = function(i) {
            if ($("#confirmPin").text() != "Clear Pin") {
                $("#regPinTxt").val($("#regPinTxt").val() + i);
            }
        };

        $scope.setPin = function() {
            if ($("#confirmPin").text() == "Clear Pin") {
                $scope.clearPin();
            } else {
                if ($scope.initialPin != "") {
                    if ($scope.initialPin == $("#regPinTxt").val()) {
                        $scope.pinNoticeText = "Pin Set";
                        $("#confirmPin").text("Clear Pin");
                        $rootScope.pinValid = true;
                    } else {
                        $scope.pinNoticeText = "Pins don't match, please re-enter your pin";
                        $scope.initialPin = "";
                        $scope.clearPin();
                    }
                } else {
                    //If Length > 4, < 16 and only numbers
                    if ($("#regPinTxt").val().length >= 4 && $("#regPinTxt").val().length <= 16 && /^\d+$/.test($("#regPinTxt").val())) {
                        $scope.initialPin = $("#regPinTxt").val();
                        $scope.clearPin();
                        $scope.pinNoticeText = "Please re-enter your pin for confirmation";
                        $scope.createRandomGrid();
                    } else {
                        $scope.pinNoticeText = "Pin must be between 4 and 16 digits long inclusive";
                        $scope.clearPin();
                    }
                }
            }
        };

        $scope.clearPin = function() {
            if ($("#confirmPin").text() == "Clear Pin") {
                $("#regPinTxt").val("");
                $scope.initialPin = "";
                $scope.pinNoticeText = "";
                $("#confirmPin").text("Set Pin");
                $rootScope.pinValid = false;
            } else {
                $("#regPinTxt").val("");
            }
        };

        $scope.init();
    }
]);