﻿angular.module("Authenticate").controller("LogPassCtrl",
[
    "$rootScope", "$scope",
    function($rootScope, $scope) {

        // Declare a proxy to reference the hub.
        var hub = $.connection.broadcastHub;

        $scope.init = function() {
            $scope.authMessage = $rootScope.authMessage;
        };

        $scope.submitPass = function() {
            $rootScope.LoadingScreen.show();
            var hashPass = $('#logPass').val().getHash();
            $.connection.hub.start().done(function() {
                hub.server.authenticatePassword(hashPass, $rootScope.user.UserReference);
            });
        };

        $rootScope.$on("authMessage", function() {
            $scope.authMessage = $rootScope.authMessage;
            $scope.$digest();
        });

        $scope.init();
    }
]);