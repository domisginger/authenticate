﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace Authenticate.Web.ApplicationServices
{
    public class PasswordEntity : TableEntity
    {
        public PasswordEntity(string userReference)
        {
            UserReference = userReference;

            PartitionKey = userReference;
            RowKey = Guid.NewGuid().ToString();
        }

        public PasswordEntity()
        {
        }

        public string UserReference { get; set; }
        public string Password { get; set; }
    }
}