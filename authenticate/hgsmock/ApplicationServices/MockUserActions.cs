﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json.Linq;

namespace Authenticate.Web.ApplicationServices
{
    public class MockUserActions : IUserActionsService
    {
        public bool RegisterNewUser(string regDataJson)
        {
            try
            {
                JObject jObject = JObject.Parse(regDataJson);
                JToken jUser = jObject["user"];
                var newUser = new MockUser
                {
                    Name = (string) jUser["name"],
                    Username = (string) jUser["username"],
                    Email = (string) jUser["email"]
                };

                //Save user
                newUser.Save();

                return newUser.Saved;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool SetAuthenticationTypes(string authTypesJson)
        {
            try
            {
                JObject jObject = JObject.Parse(authTypesJson);
                JToken jUser = jObject["register"];

                var name = (string) jUser["name"];
                var username = (string) jUser["username"];
                var email = (string) jUser["email"];

                MockUser user = GetUserFromUsername(username);

                //If user changed user or email in reg process
                if (user == null || name != user.Name || (string) jUser["email"] != user.Email)
                {
                    user = new MockUser {Name = name, Username = username, Email = email};
                }

                JToken authTypes = jUser["authTypes"];

                if ((string) authTypes["password"] != null)
                {
                    var pass = (string) authTypes["password"];
                    var tempPass = new MockPassword {UserReference = user.UserReference, Pasword = pass};
                    tempPass.Save();

                    user.AuthTypes.Add("PASSWORD");
                }

                if ((string) authTypes["pin"] != null)
                {
                    var pin = (string) authTypes["pin"];
                    var tempPin = new MockPin {UserReference = user.UserReference, Pin = pin};
                    tempPin.Save();

                    user.AuthTypes.Add("PIN");
                }

                if ((string) authTypes["barcode"] != null)
                {
                    //Barcode already saved when generated

                    user.AuthTypes.Add("BARCODE");
                }

                if ((string) authTypes["hand"] != null)
                {
                    //TODO:

                    user.AuthTypes.Add("HANDGEO");
                }

                if ((string) authTypes["face"] != null)
                {
                    //TODO:

                    user.AuthTypes.Add("FACEREC");
                }

                //Save user
                user.Update();

                return user.Saved;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool RemoveUser(string userJson)
        {
            throw new NotImplementedException();
        }

        public MockUser GetUserFromUsername(string username) //TODO: or Email?
        {
            try
            {
                CloudTable table = AzureHelper.GetTable("users");

                TableQuery<UserEntity> query =
                    new TableQuery<UserEntity>().Where(
                        TableQuery.GenerateFilterCondition("Username", QueryComparisons.Equal, username));

                //TODO: Will Only ever be one as username will be unique (DO THIS)
                foreach (MockUser user in from entity in table.ExecuteQuery(query)
                    let authTypes = entity.AuthTypes.Split('|').ToList()
                    select
                        new MockUser(entity.UserReference)
                        {
                            Name = entity.Name,
                            Username = username,
                            Email = entity.Email,
                            AuthTypes = authTypes,
                            UserReference = entity.UserReference
                        })
                {
                    return user;
                }

                Console.WriteLine("User not found");
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public UserEntity GetUserEntityFromRef(string userRef)
        {
            try
            {
                CloudTable table = AzureHelper.GetTable("users");

                TableQuery<UserEntity> query =
                    new TableQuery<UserEntity>().Where(
                        TableQuery.GenerateFilterCondition("UserReference", QueryComparisons.Equal, userRef));

                // Will Only ever be one as userref is guid
                IEnumerable<UserEntity> user = table.ExecuteQuery(query);
                return user == null ? null : user.FirstOrDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}