﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace Authenticate.Web.ApplicationServices
{
    public class UserEntity : TableEntity
    {
        public UserEntity(string username)
        {
            Username = username;

            PartitionKey = username;
            RowKey = Guid.NewGuid().ToString();
        }

        public UserEntity()
        {
        }

        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string UserReference { get; set; }
        public string AuthTypes { get; set; }
    }
}