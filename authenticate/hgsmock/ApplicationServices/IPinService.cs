﻿namespace Authenticate.Web.ApplicationServices
{
    public interface IPinService
    {
        bool Validate(string pin, string userRef);

        void Save();

        PinEntity GetPin(string userRef);
    }
}