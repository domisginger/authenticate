﻿namespace Authenticate.Web.ApplicationServices
{
    public interface IUserService
    {
        void Save();

        void Update();
    }
}