﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using HashLib;
using MessagingToolkit.QRCode.Codec;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using ZXing;

namespace Authenticate.Web.ApplicationServices
{
    public class MockBarcode : IBarcodeService
    {
        public MockBarcode()
        {
            Saved = false;
        }

        public string UserReference { get; set; }

        public string Barcode { get; set; }

        public string QrText { get; set; }

        public bool Saved { get; set; }

        public string FindBarcode(string imageData)
        {
            Result result = null;

            try
            {
                Bitmap image = ImageHelper.Base64StringToBitmap(imageData);

                var reader = new BarcodeReader
                {
                    PossibleFormats = new List<BarcodeFormat>
                    {
                        BarcodeFormat.QR_CODE
                    }
                };
                result = reader.Decode(image);
            }
            catch (Exception e)
            {
                result = null;
            }

            return result == null ? null : result.Text;
        }

        public void Save()
        {
            Debug.WriteLine("MockBarcode Save:");
            Debug.WriteLine(UserReference);
            try
            {
                CloudBlockBlob blob = AzureHelper.GetBlob("barcodes", UserReference);
                blob.UploadText(Barcode);

                CloudTable table = AzureHelper.GetTable("barcodes");

                IHash hash = HashFactory.Crypto.SHA3.CreateKeccak512();
                HashResult res = hash.ComputeString(QrText, Encoding.ASCII);
                string hashQrText = res.ToString();

                // Create a new customer entity.
                var bar = new BarcodeEntity(UserReference)
                {
                    UserReference = UserReference,
                    QrText = hashQrText
                };

                // Create the TableOperation that inserts the customer entity.
                TableOperation insertOperation = TableOperation.Insert(bar);

                // Execute the insert operation.
                table.Execute(insertOperation);

                Saved = true;
            }
            catch (Exception e)
            {
                Saved = false;
            }


            Saved = true;
        }

        public bool Validate(string userRef, string imageData)
        {
            string bar = FindBarcode(imageData);

            if (bar != null)
            {
                string existing = GetBarcodeEntity(userRef).QrText;

                IHash hash = HashFactory.Crypto.SHA3.CreateKeccak512();
                HashResult res = hash.ComputeString(bar, Encoding.ASCII);
                string barHash = res.ToString();

                return barHash == existing;
            }

            return false;
        }

        public BarcodeEntity GetBarcodeEntity(string userRef)
        {
            try
            {
                CloudTable table = AzureHelper.GetTable("barcodes");

                TableQuery<BarcodeEntity> query =
                    new TableQuery<BarcodeEntity>().Where(
                        TableQuery.GenerateFilterCondition("UserReference", QueryComparisons.Equal, userRef));

                // Will Only ever be one as userRef is guid
                IEnumerable<BarcodeEntity> barcode = table.ExecuteQuery(query);
                return barcode.FirstOrDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public string GenerateQr(string userRef)
        {
            UserReference = userRef;

            // Generate QR, qrText will be embedded in qr code, but the hashed vn saved for comparison, image must be saved incase lost or re-needed
            var encoder = new QRCodeEncoder();
            QrText = Guid.NewGuid() + "|" + userRef + "|" + Guid.NewGuid();
            Bitmap img = encoder.Encode(QrText);

            // Save to string
            var stream = new MemoryStream();
            img.Save(stream, ImageFormat.Bmp);
            byte[] bytes = stream.ToArray();
            string bar = Convert.ToBase64String(bytes);

            Barcode = bar;

            Save();

            return Saved ? Barcode : "";
        }

        public string GetBarcodeImage(string userRef)
        {
            try
            {
                CloudBlockBlob blob = AzureHelper.GetBlob("barcodes", userRef);
                string barcode = blob.DownloadText();

                return barcode;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}