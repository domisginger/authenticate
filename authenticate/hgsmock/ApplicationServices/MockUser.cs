﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace Authenticate.Web.ApplicationServices
{
    public class MockUser : IUserService
    {
        private readonly MockUserActions mockUserActions;

        public MockUser()
        {
            Saved = false;
            UserReference = Guid.NewGuid().ToString();
            AuthTypes = new List<string>();
            mockUserActions = new MockUserActions();
        }

        public MockUser(string userReference)
        {
            Saved = true;
            UserReference = userReference;
            AuthTypes = new List<string>();
            mockUserActions = new MockUserActions();
        }

        public string Name { get; set; }

        public string UserReference { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public List<string> AuthTypes { get; set; }

        public bool Saved { get; set; }

        public void Save()
        {
            Debug.WriteLine("MockUser Save:");
            Debug.WriteLine(UserReference);

            //TODO: Hash Username?

            try
            {
                CloudTable table = AzureHelper.GetTable("users");

                string allAuthTypes = AuthTypes.Aggregate("", (current, type) => current + (type + "|"));

                // Create a new customer entity.
                var user = new UserEntity(Username)
                {
                    Name = Name,
                    UserReference = UserReference,
                    Email = Email,
                    AuthTypes = allAuthTypes
                };

                // Create the TableOperation that inserts the customer entity.
                TableOperation insertOperation = TableOperation.Insert(user);

                // Execute the insert operation.
                table.Execute(insertOperation);

                Saved = true;
            }
            catch (Exception e)
            {
                Saved = false;
            }
        }

        public void Update()
        {
            Debug.WriteLine("MockUser Update:");
            Debug.WriteLine(UserReference);
            Debug.WriteLine("AuthTypes:");
            foreach (string type in AuthTypes)
            {
                Debug.WriteLine(type);
            }

            //TODO: Hash Username?

            try
            {
                CloudTable table = AzureHelper.GetTable("users");

                string allAuthTypes = AuthTypes.Aggregate("", (current, type) => current + (type + "|"));
                UserEntity user = mockUserActions.GetUserEntityFromRef(UserReference);

                TableOperation retrieveOperation = TableOperation.Retrieve<UserEntity>(user.PartitionKey, user.RowKey);
                TableResult retrievedResult = table.Execute(retrieveOperation);
                var updateEntity = (UserEntity) retrievedResult.Result;

                if (updateEntity != null)
                {
                    updateEntity.Name = Name;
                    updateEntity.Email = Email;
                    updateEntity.AuthTypes = allAuthTypes;

                    TableOperation updateOperation = TableOperation.Replace(updateEntity);
                    table.Execute(updateOperation);

                    Saved = true;
                }
                else
                    Saved = false;
            }
            catch (Exception e)
            {
                Saved = false;
            }
        }
    }
}