﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace Authenticate.Web.ApplicationServices
{
    public class BarcodeEntity : TableEntity
    {
        public BarcodeEntity(string userReference)
        {
            UserReference = userReference;

            PartitionKey = userReference;
            RowKey = Guid.NewGuid().ToString();
        }

        public BarcodeEntity()
        {
        }

        public string UserReference { get; set; }

        public string QrText { get; set; }
    }
}