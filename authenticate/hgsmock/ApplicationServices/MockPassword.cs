﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace Authenticate.Web.ApplicationServices
{
    public class MockPassword : IPasswordService
    {
        public MockPassword()
        {
            Saved = false;
        }

        public string UserReference { get; set; }

        public string Pasword { get; set; }

        public bool Saved { get; set; }

        public void Save()
        {
            Debug.WriteLine("MockPassword Save:");
            Debug.WriteLine(UserReference);

            try
            {
                CloudTable table = AzureHelper.GetTable("passwords");

                // Create a new customer entity.
                var pass = new PasswordEntity(UserReference)
                {
                    UserReference = UserReference,
                    Password = Pasword
                };

                // Create the TableOperation that inserts the customer entity.
                TableOperation insertOperation = TableOperation.Insert(pass);

                // Execute the insert operation.
                table.Execute(insertOperation);

                Saved = true;
            }
            catch (Exception e)
            {
                Saved = false;
            }

            Saved = true;
        }

        public bool Validate(string password, string userRef)
        {
            PasswordEntity storedPinEntity = GetPassword(userRef);
            return password == storedPinEntity.Password;
        }

        public PasswordEntity GetPassword(string userRef)
        {
            try
            {
                CloudTable table = AzureHelper.GetTable("passwords");

                TableQuery<PasswordEntity> query =
                    new TableQuery<PasswordEntity>().Where(
                        TableQuery.GenerateFilterCondition("UserReference", QueryComparisons.Equal, userRef));

                // Will Only ever be one as userRef is guid
                IEnumerable<PasswordEntity> pass = table.ExecuteQuery(query);
                return pass.FirstOrDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}