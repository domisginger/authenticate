﻿using Microsoft.WindowsAzure.Storage.Table;

namespace Authenticate.Web.ApplicationServices
{
    public class FaceRecEntity : TableEntity
    {
        public FaceRecEntity(string userReference)
        {
            UserReference = userReference;

            PartitionKey = userReference;
            RowKey = userReference;
        }

        public FaceRecEntity()
        {
        }

        public string UserReference { get; set; }

        public int TrainingImages { get; set; }
    }
}