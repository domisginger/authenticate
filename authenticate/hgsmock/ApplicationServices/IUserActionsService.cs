﻿namespace Authenticate.Web.ApplicationServices
{
    public interface IUserActionsService
    {
        bool RegisterNewUser(string regDataJson); // Registers simple info to set up a user

        bool SetAuthenticationTypes(string authTypesJson); // Json of all authentication types associated with user

        bool RemoveUser(string userJson);

        MockUser GetUserFromUsername(string username);

        UserEntity GetUserEntityFromRef(string userRef);
    }
}