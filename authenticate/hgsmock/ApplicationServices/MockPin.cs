﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace Authenticate.Web.ApplicationServices
{
    public class MockPin : IPinService
    {
        public MockPin()
        {
            Saved = false;
        }

        public string UserReference { get; set; }

        public string Pin { get; set; }

        public bool Saved { get; set; }

        public void Save()
        {
            Debug.WriteLine("MockPin Save:");
            Debug.WriteLine(UserReference);
            try
            {
                CloudTable table = AzureHelper.GetTable("pins");

                // Create a new customer entity.
                var pin = new PinEntity(UserReference)
                {
                    UserReference = UserReference,
                    Pin = Pin
                };

                // Create the TableOperation that inserts the customer entity.
                TableOperation insertOperation = TableOperation.Insert(pin);

                // Execute the insert operation.
                table.Execute(insertOperation);

                Saved = true;
            }
            catch (Exception e)
            {
                Saved = false;
            }


            Saved = true;
        }

        public bool Validate(string pin, string userRef)
        {
            PinEntity storedPinEntity = GetPin(userRef);
            return pin == storedPinEntity.Pin;
        }

        public PinEntity GetPin(string userRef)
        {
            try
            {
                CloudTable table = AzureHelper.GetTable("pins");

                TableQuery<PinEntity> query =
                    new TableQuery<PinEntity>().Where(
                        TableQuery.GenerateFilterCondition("UserReference", QueryComparisons.Equal, userRef));

                // Will Only ever be one as userRef is guid
                IEnumerable<PinEntity> pin = table.ExecuteQuery(query);
                return pin.FirstOrDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}