﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Authenticate.Web.Controllers;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Authenticate.Web.ApplicationServices
{
    public interface IFaceRecService
    {
        RegistrationReturn RegisterFace(string userRef, string imageData);

        bool Validate(string userRef, string imageData);

        void DetectFace(Image<Bgr, Byte> image, List<Rectangle> faces, List<Rectangle> eyes);

        EigenObjectRecognizer TrainFaceRec(string userRef);

        void SaveTrainingImage(string userRef, string imageData);

        FaceRecEntity GetFaceRecEntity(string userRef);
    }
}