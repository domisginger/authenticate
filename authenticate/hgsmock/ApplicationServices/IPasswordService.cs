﻿namespace Authenticate.Web.ApplicationServices
{
    public interface IPasswordService
    {
        bool Validate(string password, string userRef);

        void Save();

        PasswordEntity GetPassword(string userRef);
    }
}