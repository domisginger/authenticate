﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Authenticate.Web.Controllers;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;

namespace Authenticate.Web.ApplicationServices
{
    public class MockFaceRec : IFaceRecService
    {
        private float EigenDistanceThreshold = 1000; //TODO: What threshold?
        private int MinTrainingImages = 8;

        public int TrainingImages { get; set; }

        public RegistrationReturn RegisterFace(string userRef, string imageData)
        {
            var registrationReturn = new RegistrationReturn
            {
                RegistrationComplete = false,
                Saved = false,
                ImageData = null
            };

            // Find Face
            Bitmap bitmapImage = ImageHelper.Base64StringToBitmap(imageData);
            var image = new Image<Bgr, byte>(bitmapImage);

            var faces = new List<Rectangle>();
            var eyes = new List<Rectangle>();
            DetectFace(image, faces, eyes);

            if (faces.Count == 1 && eyes.Count == 2)
                // Ignore if no faces or two many faces in image, and check both eyes fully visible
            {
                // Save face
                SaveTrainingImage(userRef, imageData);

                registrationReturn.Saved = true;

                // Add rectangle face onto image and send imageData
                image.Draw(faces[0], new Bgr(Color.Red), 2);

                Bitmap img = image.ToBitmap();
                var stream = new MemoryStream();
                img.Save(stream, ImageFormat.Bmp);
                byte[] bytes = stream.ToArray();
                registrationReturn.ImageData = Convert.ToBase64String(bytes);

                if (TrainingImages >= MinTrainingImages)
                {
                    registrationReturn.RegistrationComplete = true;
                    return registrationReturn;
                }
            }

            return registrationReturn;
        }

        public bool Validate(string userRef, string imageData)
        {
            // Find Face
            Bitmap bitmapImage = ImageHelper.Base64StringToBitmap(imageData);
            var image = new Image<Bgr, byte>(bitmapImage);

            var faces = new List<Rectangle>();
            var eyes = new List<Rectangle>();
            DetectFace(image, faces, eyes);

            if (faces.Count == 1 && eyes.Count == 2)
                // Ignore if no faces or two many faces in image, and check both eyes fully visible
            {
                // Put in check that face registeration has worked
                try
                {
                    TrainingImages = GetFaceRecEntity(userRef).TrainingImages;
                }
                catch (Exception e)
                {
                }

                // Check theres enough images for a match
                if (TrainingImages >= MinTrainingImages)
                {
                    EigenObjectRecognizer imageRecognizer = TrainFaceRec(userRef);

                    Image<Gray, byte> grayImage = image.Convert<Gray, Byte>();
                    float[] dist = imageRecognizer.GetEigenDistances(grayImage);

                    // Get lowest Eigen Distance
                    float eigenDistance = dist[0];
                    for (int i = 1; i < dist.Length; i++)
                    {
                        if (dist[i] < eigenDistance)
                        {
                            eigenDistance = dist[i];
                        }
                    }

                    return eigenDistance < EigenDistanceThreshold;
                }
            }

            return false;
        }

        public void DetectFace(Image<Bgr, Byte> image, List<Rectangle> faces, List<Rectangle> eyes)
        {
            // Read the HaarCascade objects
            using (
                var face =
                    new CascadeClassifier(AppDomain.CurrentDomain.BaseDirectory +
                                          @"\HaarCascades\haarcascade_frontalface_default.xml"))
            using (
                var eye =
                    new CascadeClassifier(AppDomain.CurrentDomain.BaseDirectory + @"\HaarCascades\haarcascade_eye.xml"))
            {
                using (Image<Gray, byte> gray = image.Convert<Gray, Byte>()) //Convert it to Grayscale
                {
                    // Normalizes brightness and increases contrast of the image
                    gray._EqualizeHist();

                    // Detect the faces  from the gray scale image and store the locations as rectangle
                    // The first dimensional is the channel, The second dimension is the index of the rectangle in the specific channel
                    Rectangle[] facesDetected = face.DetectMultiScale(
                        gray,
                        1.1,
                        10,
                        new Size(20, 20),
                        Size.Empty);
                    faces.AddRange(facesDetected);

                    foreach (Rectangle f in facesDetected)
                    {
                        // Set the region of interest on the faces
                        gray.ROI = f;
                        Rectangle[] eyesDetected = eye.DetectMultiScale(
                            gray,
                            1.1,
                            10,
                            new Size(20, 20),
                            Size.Empty);
                        gray.ROI = Rectangle.Empty;

                        foreach (Rectangle e in eyesDetected)
                        {
                            Rectangle eyeRect = e;
                            eyeRect.Offset(f.X, f.Y);
                            eyes.Add(eyeRect);
                        }
                    }
                }
            }
        }

        public EigenObjectRecognizer TrainFaceRec(string userRef)
        {
            var grayImages = new Image<Gray, Byte>[TrainingImages];
            for (int i = 0; i < TrainingImages; i++)
            {
                string base64String = AzureHelper.GetBlob("facerec", userRef + i).DownloadText();
                Bitmap bitmapImage = ImageHelper.Base64StringToBitmap(base64String);

                var img = new Image<Bgr, byte>(bitmapImage);
                Image<Bgr, byte> newImg = img.Resize(128, 120, INTER.CV_INTER_LINEAR, false);
                grayImages[i] = newImg.Convert<Gray, Byte>();
            }

            var termCrit = new MCvTermCriteria(0.001);

            //TODO: Books version
            return new EigenObjectRecognizer(grayImages, 5000, ref termCrit);

            // If imageRecognizer != null; It worked

            //TODO: EMGU.cv version
            //return new Emgu.CV.EigenObjectRecognizer(grayImages, ref termCrit);            
        }

        public void SaveTrainingImage(string userRef, string imageData)
        {
            Debug.WriteLine("MockFaceRec Save Training Image:");
            Debug.WriteLine(userRef);
            try
            {
                FaceRecEntity faceRecEntity = GetFaceRecEntity(userRef);
                TrainingImages = faceRecEntity != null ? faceRecEntity.TrainingImages : 0;

                // One blob created for each training image
                CloudBlockBlob blob = AzureHelper.GetBlob("facerec", userRef + TrainingImages);
                blob.UploadText(imageData);

                TrainingImages ++;

                // From here on will insert an entity to keep track of number of training images, or will update an exiting one.
                CloudTable table = AzureHelper.GetTable("facerec");

                //TODO: Hash Face Detection Results
                //var hash = HashFactory.Crypto.SHA3.CreateKeccak512();
                //var res = hash.ComputeString(this.QrText, System.Text.Encoding.ASCII);
                //var hashQrText = res.ToString();

                // Create a customer entity.
                var face = new FaceRecEntity(userRef)
                {
                    UserReference = userRef,
                    TrainingImages = TrainingImages
                };

                // Create the TableOperation that inserts the customer entity.
                TableOperation insertOperation = TableOperation.InsertOrReplace(face);

                // Execute the insert operation.
                table.Execute(insertOperation);
            }
            catch (Exception e)
            {
            }
        }

        public FaceRecEntity GetFaceRecEntity(string userRef)
        {
            try
            {
                CloudTable table = AzureHelper.GetTable("facerec");

                TableQuery<FaceRecEntity> query =
                    new TableQuery<FaceRecEntity>().Where(
                        TableQuery.GenerateFilterCondition("UserReference", QueryComparisons.Equal, userRef));

                // Will Only ever be one as userRef is guid
                IEnumerable<FaceRecEntity> faecRec = table.ExecuteQuery(query);
                return faecRec.FirstOrDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}