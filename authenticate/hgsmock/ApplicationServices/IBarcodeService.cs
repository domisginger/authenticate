﻿namespace Authenticate.Web.ApplicationServices
{
    public interface IBarcodeService
    {
        string FindBarcode(string imageData); // Finds barcode from video stream and returns code/null

        bool Validate(string userRef, string imageData);

        string GenerateQr(string userRef);

        BarcodeEntity GetBarcodeEntity(string userRef);

        void Save();
    }
}