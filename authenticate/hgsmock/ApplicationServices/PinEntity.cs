﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace Authenticate.Web.ApplicationServices
{
    public class PinEntity : TableEntity
    {
        public PinEntity(string userReference)
        {
            UserReference = userReference;

            PartitionKey = userReference;
            RowKey = Guid.NewGuid().ToString();
        }

        public PinEntity()
        {
        }

        public string UserReference { get; set; }
        public string Pin { get; set; }
    }
}