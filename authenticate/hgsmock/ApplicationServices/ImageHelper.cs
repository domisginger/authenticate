﻿using System;
using System.Drawing;
using System.IO;

namespace Authenticate.Web.ApplicationServices
{
    public class ImageHelper
    {
        public static Bitmap Base64StringToBitmap(string base64String)
        {
            byte[] byteArrayIn = Convert.FromBase64String(base64String);
            var ms = new MemoryStream(byteArrayIn);
            Image bmpReturn = Image.FromStream(ms);
            return (Bitmap) bmpReturn;
        }
    }
}