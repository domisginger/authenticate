﻿using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Authenticate.Web.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            // Return JSON by default.  Can still get xml if you pass text/xml in the request Accept header
            MediaTypeHeaderValue appXmlType =
                config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            config.EnsureInitialized();
        }
    }
}