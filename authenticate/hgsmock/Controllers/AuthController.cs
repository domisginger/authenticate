﻿using System.Web.Http;
using Authenticate.Web.ApplicationServices;

namespace Authenticate.Web.Controllers
{
    public class AuthController : ApiController
    {
        private readonly IBarcodeService Barcode;
        private readonly IFaceRecService FaceRec;

        public AuthController()
        {
            Barcode = new MockBarcode();
            FaceRec = new MockFaceRec();
        }

        [Route("api/auth")]
        public bool Post([FromBody] AuthStreamData input)
        {
            string userRef = input.UserReference;
            string imageData = input.ImageData;
            string authType = input.AuthType;

            if (userRef != null && imageData != null)
            {
                if (authType == "FACEREC")
                {
                    return FaceRec.Validate(userRef, imageData);
                }
                if (authType == "HANDGEO")
                {
                    //TODO:
                }
                else if (authType == "BARCODE")
                {
                    return Barcode.Validate(userRef, imageData);
                }
            }

            return false;
        }
    }
}