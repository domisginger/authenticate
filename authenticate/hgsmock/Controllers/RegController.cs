﻿using System.Web.Http;
using Authenticate.Web.ApplicationServices;

namespace Authenticate.Web.Controllers
{
    public class RegController : ApiController
    {
        private readonly IFaceRecService FaceRec;

        public RegController()
        {
            FaceRec = new MockFaceRec();
        }

        [Route("api/reg")]
        public RegistrationReturn Post([FromBody] RegStreamData input)
        {
            string username = input.Username;
            string imageData = input.ImageData;
            string authType = input.AuthType;

            if (username != null && imageData != null)
            {
                var userActions = new MockUserActions();
                string userRef = userActions.GetUserFromUsername(username).UserReference;

                if (authType == "FACEREC")
                {
                    RegistrationReturn faceRecReturn = FaceRec.RegisterFace(userRef, imageData);

                    return faceRecReturn;
                }
                if (authType == "HANDGEO")
                {
                    //TODO:
                }
            }

            return null;
        }
    }

    public class RegistrationReturn
    {
        public bool RegistrationComplete { get; set; }

        public bool Saved { get; set; }

        public string ImageData { get; set; }
    }
}