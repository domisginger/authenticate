﻿namespace Authenticate.Web.Controllers
{
    public class AuthStreamData
    {
        public string UserReference { get; set; }

        public string ImageData { get; set; }

        public string AuthType { get; set; }
    }

    public class RegStreamData
    {
        public string Username { get; set; }

        public string ImageData { get; set; }

        public string AuthType { get; set; }
    }
}