﻿angular.module("Authenticate").controller("RegBarCtrl",
[
    "$rootScope", "$scope",
    function($rootScope, $scope) {

        // Declare a proxy to reference the hub.
        var hub = $.connection.broadcastHub;

        $scope.barImg = "/Images/loading1.gif";

        $scope.init = function() {
            $scope.generateQr($rootScope.user.username);
        };

        $scope.generateQr = function(username) {
            $rootScope.LoadingScreen.show();
            $.connection.hub.start().done(function() {
                hub.server.generateQr(username);
            });
        };

        hub.client.generateQrSuccessful = function(success, barcode) {
            $rootScope.LoadingScreen.hide();
            if (success) {
                // Display QR and options to send/print etc
                $scope.barImg = "data:image/png;base64," + barcode;
                $scope.$digest(); // To display image as soon as it loads
            } else {
                alert("Registration Error - Please try again.");
            }
        };

        $scope.init();
    }
]);