﻿angular.module("Authenticate").controller("RegPassCtrl",
[
    "$rootScope", "$scope",
    function($rootScope, $scope) {

        $scope.passNoticeText = "";

        $scope.passError = false;
        $scope.validate = function(id) {
            $rootScope.passValid = false;
            if (id == "regPass1") {
                $scope.passError = false;
                $scope.passNoticeText = "";

                var pwd = $("#regPass1").val();
                if (pwd.length < 8) {
                    $scope.passNoticeText += "Password should be 8 characters or longer.";
                    $scope.passError = true;
                }
                if (pwd.indexOf($rootScope.user.username) != -1) {
                    $scope.passNoticeText += "Password must not contain username.";
                    $scope.passError = true;
                }
                var re = /[0-9]/;
                if (!re.test(pwd)) {
                    $scope.passNoticeText += "Error: password must contain at least one number (0-9)!";
                    $scope.passError = true;
                }
                re = /[a-z]/;
                if (!re.test(pwd)) {
                    $scope.passNoticeText += "Error: password must contain at least one lowercase letter (a-z)!";
                    $scope.passError = true;
                }
                re = /[A-Z]/;
                if (!re.test(pwd)) {
                    $scope.passNoticeText += "Error: password must contain at least one uppercase letter (A-Z)!";
                    $scope.passError = true;
                }
                re = /^[a-zA-Z0-9]*$/;
                if (re.test(pwd)) {
                    $scope.passNoticeText += "Error: password must contain at least one special character!";
                    $scope.passError = true;
                }
            } else {
                $scope.passNoticeText = "";
                if ($("#regPass1").val() == $("#regPass2").val() && !$scope.passError) {
                    $rootScope.passValid = true;
                    $scope.passNoticeText = "Password Valid";
                } else {
                    $scope.passNoticeText += "Passwords do not match!";
                }
            }
        };


    }
]);