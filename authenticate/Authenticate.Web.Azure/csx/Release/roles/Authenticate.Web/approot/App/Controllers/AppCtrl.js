﻿angular.module('Authenticate').controller('AppCtrl',
[
    "$window", "$rootScope", "$scope", "$http",
    function($window, $rootScope, $scope, $http) {

//################
//###   MISC   ###
//################
        $rootScope.LoadingScreen = $('<div style="position:fixed;top:0;left:0;right:0;bottom:0;z-index:10000;"><img style="position:absolute;top:2%;right:20%;width:3%" src="/Images/loading1.gif" alt="Loading..." /></div>')
            .appendTo($('body')).hide();

        // Declare a proxy to reference the hub. //TODO: Move this into  separate directive i.e. unselectable
        var hub = $.connection.broadcastHub;

        $rootScope.passValid = false;
        $rootScope.pinValid = false;

        $rootScope.user = {};
        $rootScope.authMessage = "";

        $scope.init = function() {
            var obj;
            if (sessionStorage.register && sessionStorage.register != "") {
                if (sessionStorage.user && sessionStorage.user != "") {
                    obj = JSON.parse(sessionStorage.user);
                    if (obj.AuthTypes == "") {
                        $rootScope.register = true;
                        obj = JSON.parse(sessionStorage.register);
                        $rootScope.user = obj.user;
                    } else {
                        $rootScope.register = false;
                        obj = JSON.parse(sessionStorage.user);
                        $rootScope.user = obj.user;
                        if (obj.user == undefined) $rootScope.user = obj;

                        if (sessionStorage.register && sessionStorage.register != "") sessionStorage.register = "";
                        sessionStorage.user = "";
                    }
                } else {
                    $rootScope.register = true;
                    obj = JSON.parse(sessionStorage.register);
                    $rootScope.user = obj.user;
                }
            } else if (sessionStorage.user && sessionStorage.user != "") {
                $rootScope.register = false;
                obj = JSON.parse(sessionStorage.user);
                $rootScope.user = obj.user;
                if (obj.user == undefined) $rootScope.user = obj;

                sessionStorage.user = "";
            } else {
                $(location).attr('href', "/index.html");
            };
        };

        String.prototype.getHash = function() {
            var temp = CryptoJS.SHA3(this.toString());
            return temp.toString();
        };


//####################
//###   REGISTER   ###
//####################
        hub.client.registerSuccessful = function(success) {
            $rootScope.LoadingScreen.hide();
            if (success) {
                //TODO: redirect to main user page / loopback to original page with secure cookie/session etc
                $(location).attr('href', "/index.html");
            } else {
                alert("Registration Error - Please try again.");
            }
        };

        $scope.registerAll = function() {

            $rootScope.LoadingScreen.show();

            var error = false;
            var alertText = "";

            var register = {
                name: $rootScope.user.name,
                username: $rootScope.user.username,
                email: $rootScope.user.email,
                authTypes: {}
            };

            if ($("[name='chkFace']")[0].checked) { //Face //TODO: LEFT Its this selector wrong

                //TODO: Validate

                //TODO: Add to list
                register.authTypes.face = "todo";
            }

            if ($("[name='chkHand']")[0].checked) { //Hand

                //TODO: Validate

                //TODO: Add to list
                register.authTypes.hand = "todo";

            }

            if ($("[name='chkBar']")[0].checked) { //Barcode
                // Users fault if they dont save the image //TODO: Give option to download on their profile
                register.authTypes.barcode = "barcode";
            }

            if ($("[name='chkPin']")[0].checked) { //Pin
                if ($rootScope.pinValid) {
                    register.authTypes.pin = $("#regPinTxt").val().getHash();
                } else {
                    alertText += "Please ensure Pin has been set, via the set pin button so it can be reentered and confirmed, or unselect Pin Authentication type.";
                    error = true;
                }
            }

            if ($("[name='chkPass']")[0].checked) { //Password
                var pass = $("#regPass1").val();
                var passConfirm = $("#regPass2").val().getHash();

                if (pass.length > 0 && passConfirm.length > 0 && $rootScope.passValid) {
                    register.authTypes.password = pass.getHash();
                } else {
                    alertText += "Please ensure both Password fields entered or unselect Password Authentication type.";
                    error = true;
                }
            }


            if (!register.authTypes.password && !register.authTypes.pin) {
                error = true;
                alertText += "At least one Authentication type that doesn't use a camera is required (Pin/Password), for devices without a camera.";
            }

            if (jQuery.isEmptyObject(register.authTypes)) {
                error = true;
                alertText += "At least one Authentication type must be selected!";
            }

            if (!error) {
                //Json
                var json = { register: register };
                var jsonToSend = JSON.stringify(json);

                //Send
                $.connection.hub.start().done(function() {
                    hub.server.registerAuthTypes(jsonToSend);
                });
            } else {
                alert(alertText);
                $rootScope.LoadingScreen.hide();
            }
        };

//#################
//###   LOGIN   ###
//#################

        $rootScope.otherAuthTypes = [];

        //TODO: Needed still?
        $rootScope.$on("authTypeChanged", function(i, newAuthType) {
            $rootScope.otherAuthTypes = [];

            if (newAuthType != "FACEREC" && $rootScope.user.AuthTypes.indexOf("FACEREC") != -1) {
                $rootScope.otherAuthTypes.push("Facial Recognition");
            }
            if (newAuthType != "HANDGEO" && $rootScope.user.AuthTypes.indexOf("HANDGEO") != -1) {
                $rootScope.otherAuthTypes.push("Hand Geometry");
            }
            if (newAuthType != "BARCODE" && $rootScope.user.AuthTypes.indexOf("BARCODE") != -1) {
                $rootScope.otherAuthTypes.push("Barcode");
            }
            if (newAuthType != "PIN" && $rootScope.user.AuthTypes.indexOf("PIN") != -1) {
                $rootScope.otherAuthTypes.push("Pin");
            }
            if (newAuthType != "PASSWORD" && $rootScope.user.AuthTypes.indexOf("PASSWORD")) {
                $rootScope.otherAuthTypes.push("Password");
            }
        });

        hub.client.authResponce = function(success) {
            $rootScope.authResponce(success);
        };

        $rootScope.authResponce = function(success) {
            $rootScope.LoadingScreen.hide();
            if (success == true) {
                //TODO: redirect to main user page / loopback to original page with secure cookie/session etc
                $(location).attr('href', "/Success.html");
            } else {
                $rootScope.authMessage = "Incorrect";
                $rootScope.$broadcast("authMessage");
                $rootScope.authMessage = "";
            }
        };
        $scope.init();
    }
]);