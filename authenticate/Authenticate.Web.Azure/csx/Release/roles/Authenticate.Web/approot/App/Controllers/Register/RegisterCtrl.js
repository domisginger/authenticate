﻿angular.module("Authenticate").controller("RegisterCtrl",
[
    "$rootScope", "$scope", "$http", "$interval", "$q",
    function($rootScope, $scope, $http, $interval, $q) {

        //TODO: Get name and username from signup page
        $scope.user = $rootScope.user;

        $scope.authType = "";

        $scope.getUserMedia = "";
        $scope.hasCam = false;
        $rootScope.Stream = false;

        var timer;
        var canceller;

        $("[name='chkFace']").bootstrapSwitch();
        $("[name='chkHand']").bootstrapSwitch();
        $("[name='chkBar']").bootstrapSwitch();
        $("[name='chkPin']").bootstrapSwitch();
        $("[name='chkPass']").bootstrapSwitch();

        $scope.init = function() {
            $("div.tab-menu > div.list-group > a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.tab > div.tab-content").removeClass("active");
                $("div.tab > div.tab-content").eq(index).addClass("active");
                $scope.getAuthType();
            });

            $scope.hasCam = $scope.hasGetUserMedia();

            if ($scope.hasCam) {
                $scope.video = document.querySelector('video'); //TODO: Temp
                navigator.getUserMedia({ audio: false, video: true }, function(stream) {
                    // There will be one video hidden but multiple Canvas's to display video for auth type.
                    $scope.video.src = window.URL.createObjectURL(stream);
                    $scope.initCam();
                }, function(err) {
                    console.log("Camera must be pluged in and permissions allowed for Authentication types requiring a camera: ", err);
                    $scope.hasCam = false;
                    $scope.initNoCam();
                });
            } else {
                alert("Authentication types requiring a camera are not supported on this device");
                console.log('Authentication types requiring a camera are not supported on this device');
                $scope.initNoCam();
            }
        };

        $scope.initNoCam = function() {
            //TODO: Only display options a user has allowed

            // Block Auth types requiing camera
            $(".authTypeVideo").removeClass("active");
            $(".authTypeVideo").click(function(e) {
                alert("Camera must be pluged in and permissions allowed for Authentication types requiring a camera.");
            });

        };

        //TODO: There may be a global var that can set authtype?
        $scope.initCam = function() {
            $rootScope.Stream = true;

            $scope.getAuthType();

            $scope.streamVideo();
        };

        $scope.getAuthType = function() {
            // Work out whats authtype to register by which tab is active
            var activeTabText = $("a.list-group-item.active")[0].innerText;

            if (activeTabText.indexOf("Facial Recognition") > -1) {
                $scope.authType = "FACEREC";
            } else if (activeTabText.indexOf("Hand Geometry") > -1) {
                $scope.authType = "HANDGEO";
            } else if (activeTabText.indexOf("Barcode") > -1) {
                $scope.authType = "BARCODE";
            } else if (activeTabText.indexOf("Pin") > -1) {
                $scope.authType = "PIN";
            } else if (activeTabText.indexOf("Password") > -1) {
                $scope.authType = "PASSWORD";
            }

            $scope.$apply();
        }; // Watch for change then end timer and start new stream
        $scope.$watch('authType', function(newVal, oldVal) {
            if (oldVal == "FACEREC" || oldVal == "HANDGEO") {
                $scope.cancel();
            }

            if (newVal == "FACEREC" || newVal == "HANDGEO") {
                $scope.streamVideo();
            }
        });

        $scope.cancel = function() {
            canceller.resolve("cancelled");
            $interval.cancel(timer);
        };

        $scope.streamVideo = function() {
            //TODO: Should be able to call record() 
            // to get access to a recorder. 
            // This recorder can then be used to access the raw data. 

            if ($rootScope.Stream) {
                var canvas = $("#canvas" + $scope.authType);
                var ctx = canvas.get()[0].getContext('2d');

                canceller = $q.defer();
                $interval.cancel(timer); //http://www.sitecrafting.com/blog/multiple-interval-javascript-problem
                timer = $interval(
                    function() {
                        // Take snapshot of current video & Paint onto canvas
                        ctx.drawImage($scope.video, 0, 0, 320, 240);

                        // Grab data from canvas
                        var imageData = canvas.get()[0].toDataURL('image/jpeg', 1.0).replace("data:image/jpeg;base64,", "");

                        // Send imagedata via hub
                        $scope.sendImage(imageData);
                    }, 1000); // Slower than when detecting
            }
        };

        $scope.sendImage = function(imageData) {
            // Send imagedata via webapi, cant use signalr due to base64 image size
            $http.post("/api/reg", JSON.stringify({
                Username: $rootScope.user.username,
                ImageData: imageData,
                AuthType: $scope.authType
            }), { timeout: canceller.promise }).success(function(responce) {

                if ($scope.authType == "FACEREC") {
                    if (responce.Saved == true) {
                        responce.ImageData = "data:image/jpeg;base64," + responce.ImageData;

                        // Find next blank image
                        if ($("#imgFace1").attr("src") == "\\Images\\user.jpg") {
                            $("#imgFace1").attr("src", responce.ImageData);
                        } else if ($("#imgFace2").attr("src") == "\\Images\\user.jpg") {
                            $("#imgFace2").attr("src", responce.ImageData);
                        } else if ($("#imgFace3").attr("src") == "\\Images\\user.jpg") {
                            $("#imgFace3").attr("src", responce.ImageData);
                        } else if ($("#imgFace4").attr("src") == "\\Images\\user.jpg") {
                            $("#imgFace4").attr("src", responce.ImageData);
                        } else if ($("#imgFace5").attr("src") == "\\Images\\user.jpg") {
                            $("#imgFace5").attr("src", responce.ImageData);
                        } else if ($("#imgFace6").attr("src") == "\\Images\\user.jpg") {
                            $("#imgFace6").attr("src", responce.ImageData);
                        } else if ($("#imgFace7").attr("src") == "\\Images\\user.jpg") {
                            $("#imgFace7").attr("src", responce.ImageData);
                        } else {
                            $("#imgFace8").attr("src", responce.ImageData);
                        }

                        if (responce.RegistrationComplete == true) {
                            $("#regFace-container").css("background-color", "palegreen");
                            // Stop Streaming
                            //TODO: But still stream to canvas
                            $scope.cancel();
                        }
                    }
                }
            });
        };

        $scope.hasGetUserMedia = function() {
            navigator.getUserMedia = navigator.getUserMedia ||
                navigator.webkitGetUserMedia ||
                navigator.mozGetUserMedia ||
                navigator.msGetUserMedia;

            return navigator.getUserMedia;
        };

        $scope.init();
    }
]);