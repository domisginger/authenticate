﻿angular.module("Authenticate").controller("LogPinCtrl",
[
    "$rootScope", "$scope",
    function($rootScope, $scope) {

        // Declare a proxy to reference the hub.
        var hub = $.connection.broadcastHub;

        $scope.otherAuthTypes = $rootScope.otherAuthTypes;
        $scope.switchAuthType = $rootScope.switchAuthType;

        $scope.initialPin = "";
        $scope.pinNoticeText = "";

        $scope.init = function() {
            $scope.authMessage = $rootScope.authMessage;

            $scope.createRandomGrid();
        };

        //TODO: Dont randomize grid for register (or only for confirm?)
        $scope.pinGrid = {};
        $scope.createRandomGrid = function() {
            var list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
            var randList = [];
            for (var i = 0; i < 10; i++) {
                var random = Math.floor(Math.random() * (list.length - 1));
                var a = list[random];
                list.splice(random, 1);
                randList.push(a);
            }

            $scope.pinGrid.pin1 = randList[0];
            $scope.pinGrid.pin2 = randList[1];
            $scope.pinGrid.pin3 = randList[2];
            $scope.pinGrid.pin4 = randList[3];
            $scope.pinGrid.pin5 = randList[4];
            $scope.pinGrid.pin6 = randList[5];
            $scope.pinGrid.pin7 = randList[6];
            $scope.pinGrid.pin8 = randList[7];
            $scope.pinGrid.pin9 = randList[8];
            $scope.pinGrid.pin10 = randList[9];
        };

        $scope.enterPin = function(i) {
            $("#logPinTxt").val($("#logPinTxt").val() + i);
        };

        $scope.submitPin = function() {
            $rootScope.LoadingScreen.show();
            var hashPin = $('#logPinTxt').val().getHash();
            $scope.clearPin();
            $.connection.hub.start().done(function() {
                hub.server.authenticatePin(hashPin, $rootScope.user.UserReference);
            });
        };

        $scope.clearPin = function() {
            $("#logPinTxt").val("");
            $scope.authMessage = "";
        };

        $rootScope.$on("authMessage", function() {
            $scope.authMessage = $rootScope.authMessage;
            $scope.$digest();
        });


        $scope.init();
    }
]);