﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="Authenticate.Web.Azure" generation="1" functional="0" release="0" Id="620f6ea4-16c5-4df3-acbe-8c720ad6d27b" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="Authenticate.Web.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="Authenticate.Web:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/LB:Authenticate.Web:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="Authenticate.Process:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/MapAuthenticate.Process:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="Authenticate.ProcessInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/MapAuthenticate.ProcessInstances" />
          </maps>
        </aCS>
        <aCS name="Authenticate.Web:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/MapAuthenticate.Web:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="Authenticate.Web:StorageConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/MapAuthenticate.Web:StorageConnectionString" />
          </maps>
        </aCS>
        <aCS name="Authenticate.WebInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/MapAuthenticate.WebInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:Authenticate.Web:Endpoint1">
          <toPorts>
            <inPortMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.Web/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapAuthenticate.Process:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.Process/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapAuthenticate.ProcessInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.ProcessInstances" />
          </setting>
        </map>
        <map name="MapAuthenticate.Web:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.Web/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapAuthenticate.Web:StorageConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.Web/StorageConnectionString" />
          </setting>
        </map>
        <map name="MapAuthenticate.WebInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.WebInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="Authenticate.Process" generation="1" functional="0" release="0" software="C:\Users\Dominic\Documents\Working CODE\Authenticate\authenticate\Authenticate.Web.Azure\csx\Debug\roles\Authenticate.Process" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="1792" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;Authenticate.Process&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;Authenticate.Process&quot; /&gt;&lt;r name=&quot;Authenticate.Web&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.ProcessInstances" />
            <sCSPolicyUpdateDomainMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.ProcessUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.ProcessFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="Authenticate.Web" generation="1" functional="0" release="0" software="C:\Users\Dominic\Documents\Working CODE\Authenticate\authenticate\Authenticate.Web.Azure\csx\Debug\roles\Authenticate.Web" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="StorageConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;Authenticate.Web&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;Authenticate.Process&quot; /&gt;&lt;r name=&quot;Authenticate.Web&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DevStore" defaultAmount="[1000,1000,1000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.WebInstances" />
            <sCSPolicyUpdateDomainMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.WebUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.WebFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="Authenticate.WebUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="Authenticate.ProcessUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="Authenticate.ProcessFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="Authenticate.WebFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="Authenticate.ProcessInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="Authenticate.WebInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="93e8f873-1c2f-4399-ae70-9b620042ca61" ref="Microsoft.RedDog.Contract\ServiceContract\Authenticate.Web.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="fffb4373-fa1a-47e7-97ea-5e75036c4601" ref="Microsoft.RedDog.Contract\Interface\Authenticate.Web:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/Authenticate.Web.Azure/Authenticate.Web.AzureGroup/Authenticate.Web:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>